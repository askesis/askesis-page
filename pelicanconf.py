#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


import pathlib

BASE_DIR = pathlib.Path(__file__).parent

AUTHOR = 'Jacek Bzdak <jacek@askesis.pl>'
SITENAME = "Jacek's Blog"

SITEURL = ''

PATH = 'content'

CONTENT = str(BASE_DIR / PATH)

TIMEZONE = 'Europe/Warsaw'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = None

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = str(BASE_DIR / 'askesis-theme')

MENUITEMS = []

ARTICLE_SAVE_AS = 'post/{date:%Y/%m}/{slug}.html'
ARTICLE_URL = ARTICLE_SAVE_AS

ARTICLE_LANG_SAVE_AS = 'post/{lang}/{date:%Y/%m}/{slug}.html'
ARTICLE_LANG_URL = ARTICLE_LANG_SAVE_AS


STATIC_PATHS = ['images', 'pdfs', 'favicon.ico']

PYGMENTS_RST_OPTIONS = {}

DEFAULT_DATE_FORMAT = "%Y-%m-%d"

SIMPLE_TRACKER_URL = "https://o5h84vss67.execute-api.eu-central-1.amazonaws.com/default/LambdaCtr"

MATH_JAX = {
    "source": '''"''' + SITEURL + '''/theme/mathjax-es5/tex-chtml-full.js"'''
}
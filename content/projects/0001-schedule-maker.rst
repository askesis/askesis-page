Worker schedule maker
=====================

:date: 2019-07-01
:tags: project,legacy-code,python,numpy
:slug: schedule-app

In this project I was extending legacy code of project that
optimized worker schedules. I did almost all of the technical
work.

What was the challenge:

* Application had a lot of technical debt at the start
  of the project. IMO I decreased amount of debt;
* Performance budget was almost spent in previous iteration,
  so I needed to make sure that I won't make things slower;
* I needed to maintain good backwards compatibility;

What went well:

* Maintaining backwards compatibility;
* Very little bugs new bugs introduced;
* Significantly reduced amount of existing bugs;

What didn't went well:

* This was a project with tight timeline, if I had
  more time (or just took more risks) I could probably
  remove much more debt, and improve performance.
* I did introduced a lot of tests, but still
  unit-test coverage is bad (most of issues will
  come out in integration tests, though, but guessing
  what went wrong was not straightforward;

`If you are interested in more technical conclusions see here <{filename}/developement/0028-how-to-handle-legacy-code.rst>`__.





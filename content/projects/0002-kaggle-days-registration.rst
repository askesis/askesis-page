Registration system for Kaggle Days
===================================

:date: 2019-07-01
:tags: project,python,django
:slug: kaggle-days-register

Kaggle Days is a very nice conference for data scientists. It's atypical
as you need to pass qualification check to attend (while this may seem
controversial major event of each conference is a offline competition, where
data-scientists solve some interesting problems to win the prizes, so
the organizers need to check if attendees will be able to meaningfully take
part in the competition).

So anyways this is not a typical conference and registration flow is:

* Someone fills a form, they get welcome e-mail;
* Their profile is evaluated and they are either
  accepted, wait-listed or rejected; (in all cases they get an e-mail)
* When accepted they get payment e-mail;
* Upon reaching maximal number of attendees we cut off the registration;

I have implemented this flow in Django.

What worked well:

* Exactly zero user-visible interface. (Registration form is done in
  WordPress)

  This was good first order assumption what speed up everything;
* I made a plug-in system for the newest event, this worked well to;
* Hand coded payment code; When I was starting the project I didn't find
  a non-overengineered, vulnerability free implementation of PayU payments
  (this is first provider we did) in python.
* People using the UI are happy;

What wasn't stellar:

* Each conference was different, and I should have used plug-in system
  earlier;


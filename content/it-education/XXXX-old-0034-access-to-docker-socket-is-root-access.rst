Access to docker socket **is** root access
==========================================

:date: 2017-11-17
:tags: debian,linux,docker
:summary:

    It turns out that ``pgdump`` command is sensitive to latency between ``pgdump``
    and ``postgresql`` database.

Docker internally uses an api to communicate between docker client (eg.
``docker`` command) and docker daemon.

If you have access to this docker socket, you effectively have root access
to the docker host. This is mentioned in the documentation, but hell, I
was suprised it was **that easy**.

Try this on your local docker machine:

.. code-block:: bash

    docker run -it --mount type=bind,src=/,destination=/host debian:latest sh

This will give you shell that has your root folder mounted on ``/host`` directory
with full read and write root access to the file system.

In other words: If you can run docker from local user, you don't need to ask
password for ``sudo``.
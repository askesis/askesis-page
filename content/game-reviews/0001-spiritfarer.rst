Spiritfarer
===========

:date: 2020-09-27
:tags: game,game-review,spiritfarer,no-challenge,easy-game

.. warning::

    **Content warnings**:

    * Spiritfarer is ultimately a game about dying and death.
    * Contains smoking characters;
    * Game is non-vegan;

Overall recommendation
----------------------

Go and buy this game.

Game overview
-------------

In Spiritfarer you play Stella, girl/woman tasked with taking over Spiritfarer
job from Charon. Spiritfarer is responsible for allowing souls to travel to
another life via The Everdoor, the trick is that soul needs to **want** to do
it, so you need to make it comfortable and happy.

Stella gets her own ship, on which she travels the waters of weird afterlife
world. Throught the game she meets spirits, befriends them, and ultimately
says farewell to them. Some of these farewells will be unexpected, others
will be confused, almost all of them will be sad.

For me the game was not challenging (in a good way!), most of the minigames
were super-easy to finish, and those that were not that easy, allowed for
in-game progress even if I fumbled a lot.

Game mechanics
--------------

Game mechanics include:

* Upgrading the ship;
* Building homes for ghosts (and other buildings) on the ship;
* Resource gathering, each resource requires you to play a minigame;
* Farming and mining;
* Doing quests;
* Light platforming;
* Keeping spirits happy by giving them food they like, and hugging them;

Game mechanics does not include:

* Meaningful fights;
* Challenge --- most of the minigames are **easy**, it's hard *not to make
  progress* even if you fumble; also platforming is not hard;
* Grinding;

.. figure:: {static}/images/game-reviews/0001/2020090818521800-F07F7B79366747AB415C6A1B4A8BBDE1.jpg
   :width: 80%
   :alt:  Stella hugging a spirit near building where you can grow grains.

   Stella hugging a spirit near building where you can grow grains.

Graphics and music
------------------

On first look this game has uses retro pixelated graphics, however this is not
entirely true. It also uses lightning effects and has very nice day-night cycle.

Spiritfarer graphics is very deliberate, there is nothing unnecessary.

Music builds the game atmosphere, and also is very good.

.. figure:: {static}/images/game-reviews/0001/2020091313362100-F07F7B79366747AB415C6A1B4A8BBDE1.jpg
   :width: 80%
   :alt:    Particularly nice sunset.

   Particularly nice sunset.

Spoilers start here
-------------------

Ultimately this game is about death, and also about meaning of life, the whole
game can be interpreted as a metaphor for Stella's life. Some out-of-game hints
suggest that Stella in life was a palliative-care nurse, and spirits she meets
during her voyage are her former patients (some of them were her friends and
family).

Ultimately Stella will go through the EVERDOOR herself. She even gets a
companion who will stay with her to the end, and there there are "post-endgame"
quests (you can do extra quests after you send all the spirits off). This last
part of the game is super-sad, you roam the empty sees, knowing that you won't
meet anybody new, you have fully upgraded ship, that just is **empty**. You
still have all homes you built for each of the visitors, you still can play
every minigame, but what is the point, if everybody in Stella's life is gone?


.. figure:: {static}/images/game-reviews/0001/2020090920505700-F07F7B79366747AB415C6A1B4A8BBDE1.jpg
   :width: 80%
   :alt: Stella learns how to meditate.

   Stella learns how to meditate.


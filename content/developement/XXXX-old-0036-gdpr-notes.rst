GDPR session notes
==================

:date: 2017-12-17
:tags: law,gdpr,oeb
:summary:

    OEB (former: Online Educa Berlin) is a major ed-tech conference. Here are my notes from
    2017 edition of said conference.

    Precisely notes from GDPR session.


This is continuation of previous blog post (OEB notes) but since I have a lot
of notes from this session, this deserves separate post.

.. warning::

    These are my own personal notes. They were **not authorized whatsoever**
    and most probably are both **factually inaccurate** and **slightly different** from
    what speakers said.

.. note::

    GRPR is abbreviated as RODO in Poland. Also often called RODOS.


Brief introduction
------------------

* GDPR stands for General Data Protection Regulation;
* It will be enforceable fully and directly from 25 V 2017;
* It applies directly to all UE institutions (no need for local parlaments to
  implement it);
* Previous regulations were passed in ~1995, and needed to be implemented, and
  was implemented differently --- hence many of the Tech companies have
  locathin there (this and lax tax rules, apparantly)

What will change (or not):

* Some possiblility for maneouver --- member states can have different legislation.
* You'll need to check local laws nevertheless.
* Possibly RODO might contratict some local laws passed in accordance with the
  previous directive.
* GDPR focuses on "informed consent" (as previous legislation). If it was up to
  me (JB) I would just plainly forbid selling PI.
* Dramatic sanctions up to 20M$ and/or total yearly turnover. Enough to topple
  big players.

Personal information
--------------------

* Any information that might lead to identification of a persion with "reasonable steps"
  (NOTE: This is about the same as currently in Poland. Another note: Example of
  "unreasonable step" breaking current state-of-the-art cryptography).
* Genetic and biometric data **is personal data**.
* Processing of PI is prohibited.

  * Unless you have either informed consent
  * legal reason to collect this data (e.g. you are an university and collect PI of students).
  * real company interest to process it, and it doesn't contradict laws and intersdts
    of people you process (I'm not sure this is indeed the case --- I might have misheard)

* It applies to every EU citizen, also for companies outside of EU.
* Companies need PI officer.
* Companies (entities!) need to prove compliance

  * Rules of processing
  * Logs of processing;
  * Ability to export data.
  * Know and prove who had access to this data.
  * **This applies to subcontractios**
  * Person can ask for a **copy of all personal data** and or **removal of all personal data**
  * There are mandatory breach notifications

* There are **required self assesments**, in case od **risky operations**, you need
  to inform controller, and mitigate all risks, if you can't you can't proceed
  with this operation.

General question we need to ask ourselves in education enviornment: "Is the data
used wisely? Do we use it to make all important decisions"?

Children in GDPR

* Children require special protection.
* There are safeguards **against** fully automated decision making (for children?)
* Processing of child data requires parent approval up to certain age.
  This age depends on country.

  Also --- this article doesn't give us **consent** to store data used to
  establishing this relationship.

  There are two uncertain/hard things:

  * How can we establish parent-child relationship? We need to use "state of the art".
  * Child varies from country to country? Which age should we use when French Child
    uses Polish service is uncertain.
* Children above consent age, **still can't agree to everything**.
* Children **care about privacy**, data processing rules need to be **understandable by them**.

GDPR is based on informed consent

Just like the old regulation

* Let me know what you are going to do;
* Keep my data secured;
* Trans-border data movements is a problem;

Big breaches mean big fines.

In education we think we need "identification" and not "privacy", this is mostly
a myth.

How to prepare to GDPR
----------------------

Leader of leading proctored exam company speaks.

* Process of being compliant with GDPR is difficult.
* In US privacy laws are not existent.
* They store data in EU. Student data does not leave EU.
* They have some tests on underage persons (High-School).
* They have different levels of proctored exams, from preventing printing, to
  full invigilation by camera, microphone, snapshot of network communication.

You need Privacy by Design:

* Contractually universities **can't force** students to use proctoring software,
  if they don't want to do it, University needs to accomodate them differently.
* PI data is stored on **university system**
* Videos of exam are stored on their system in **encrypted form**, but
  only the University has the decryption keys.

Q/A
---

How to prepare for GDPR?
************************

* Identify what data you have, where it is stored and when is it deleted.

  .. note::

     Probably backups are another matter? How to manage deletion of PI data
     from backups

* Ask yourself: "Do you manage PI", or just collect it?
* Do you have people responsible for management of PI?
* Start with reading and amending your contracts;
* Talk to your lawyers;
* Protect employee records;
* Check if your subcontractors are compliant (you can be liable for them!);
* Look up your current personal data rules;

Did this US company use this process
************************************

* They switched they law company to EU based one, that was specializing in
  RODO :)
* They had problems with 72h notification rule. After you discover a breach
  you have 72hours to:

  * Notify your controller;
  * Fix the bugs that lead to breach;
  * Find what data was stolen, and whose PI was affected;
  * Try to find the perpetrator;

  This requires an on-call team that is specializing in this kind of things.

Where to start with handling children's data
********************************************

* It's not "starts on 25'th of May" kind of regulation. If you have gaping holes
  now, probably you are not compliant with current regulation.
* This is not something you can **dump on IT guy** this is mostly a
  **managerial matter**.
* There is problem with "age of consent for children"
* Using third-party programs, like: "Google Classroom" can lead you to be
  not-compliant.

Should data be cleaned when they leave the school
*************************************************

* You need a **purpose** to store data, if you lose the purpose you need to
  **erease**
* Schools generally need to keep records due to local laws, and this is purpose
  enough (but only for data that they **need to keep**).
* You need a purpose to **give data to third party**. You shouldn't give all
  profile data to third party developer. You shouldn't give out child photo
  or home address, to subcontractor without very good reason
  (this happens sometimes).

How can we check for compliance
*******************************

* Not legally required;
* There is no certification ready (probably will be);
* There are existing certifications in Germany;
* There are a lot of **ISO** standards to follow;

You need to have a lot of documents for controller.

How do you know you are ready
*****************************

* This is hard;
* You are never "compliant" this is a process.

  You might be complaint but someone will steal you network traffic with
  hacked coffe maker and or printer.

  .. note::

    There are some points to make here:

    * Probably if something can be a dump (not connected to Internet) machine
      it most probably should be.
    * Network separation is the king.
    * Probably all data in your network needs to be encrypted, so passive
      attacker gets nothing (and active attacker is easier to detect!)
    * Having network as only security boundary is a flawed concept.


* Ask yourself: "Is your business created with privacy in mind, if not you need
  to change that"
* Use lawyers; They can give you self-assessment.

How to start compliance preparations
************************************

* Now might be too late, this might take couple of years for some companies.
* You might go to Conroller directly, but for some reason, companies usually
  avoid that.
* Start with complance with courrent law.

Start with documentation:

* What kind of data do you store?
* Where?
* Who is responsible?
* Do you have consents to store this data?

Can I store data for future uses
********************************

* It's difficult to use it that way.
* You need to know **how** will you use it.
* Collect consents!
* This kills startup idea: "Let's collect piles of data, and then figure out
  how to sell it".

Will anonymizing the data help
******************************

* If you collected the data legally, anonymizing will help.
* Anonymizing is **hard**. You might remove all PI metadata, but in one of
  homework essays your student will put their name --- in this case data is
  no longer anonymous.
* Probably you'll need consent for anonymizing and using anonymous data.

What if I provide the data in good faith and some costumer will make a breach
*****************************************************************************

* You are liable according to GDPR;
* You can then sue them;
* You need to have paper trail, and be accountable;

What if someone wants to delete data, that is a part of neural network
**********************************************************************

* If it's anonymous you can keep it.

Age verification
****************

* It's hard;
* You can't ask for birthday, and hope for the best;
* Do video of child and parent, and verify facial similarity;
* You can't easily store data used for the verification, you can't use GDPR
  as an excuse to store compliance data.
* There are some restrictions on child targeted marketing.

How to store data that is sent by e-mail
****************************************

Example: some student send's me he is dyslectic and then he needs extra time.

* Probably you need to store e-mail due to data retention reasons.
* Then you need to delete the e-mail.
* If you get "long term data" (e.g. dyslexia) you can store it in some long term
  system (delete when student leaves your institution).
* If you get "one shot information" from the student (he asks for extension as
  he is fired from work) just act on the information and don't store it.

Disclaimers:

* Keep your data on **work laptop**! Data shouldn't **leave your systems**;
* This includes e-mail forwarding;

What about data portability
***************************

What if student comes to me and says he wants a copy of all his PI?

* Good question;
* Keep data in simplest format;

Really big changes
------------------

* You might be liable for subcontractors that are non-compliant.
* You might be liable for breaches at third parties that you did provide data.
* You need to give customer full copy of his data;
* You need to remove data when he user asks for removal (unless you have legal
  grounds to jeep the data);
* Big fines for breaches;

How to store results of javascript study in Qualtrics survey
============================================================

:date: 2019-06-23
:slug: how-to-store-results-from-a-javascript-study-in-qualtrics
:tags: webdev,qualtrics,javascript

To store results from a custom javascript study in Qualtrics you'll need to:

1. Create (empty) "Embedded data" where results will be stored;
2. In the JavaScript code store to this embedded data;
3. Figure out with your client how to extract the data for them :)

Create "Embedded data"
----------------------

1. Open your survey in the Qualtrics UI.
2. Click on Survey Flow
3. Click "Add new Element" -> "Embedded Data"
4. Set a name for this element, like: ``study-data``
5. Set value to `{}`

   .. figure:: {static}/images/0027-qualtrics-embedded-data.png
      :width: 100%
      :alt: Your "Embedded Data" node should look like that

      Your "Embedded Data" node should look like that

6. Make sure this is the first node in the survey;

Write to this Embedded data node
--------------------------------

When your study is done just:

.. code-block:: javascript

    var data;

    // data = load from your study

    Qualtrics.SurveyEngine.setEmbeddedData("study-data", data);


Final note
----------

If this sound like black magic (or just something you'd rather avoid doing),
do get in touch I might help you with whatever you need :). My address is ``jacek@askesis.pl``.

Hosting providers list
======================

:date: 2019-02-04
:tags: services,hosting,list


A work in progress list of services I evaluated. Will update it from time to time;

Here is a list of services I used/evaluated:

Hosting providers
-----------------

* https://aws.amazon.com/

  Pros:

  - Biggest cloud provider;
  - Biggest product portfolio;
  - Has kubernetes;

  Cons:

  - Using their web panel is a pain;
  - Their web admin panel panel is so bad it deserves second point;
  - Pricing is, well, complicated;
  - Non-EU based company;

* https://cloud.google.com/

  Pros:

  - All services are well integrated, and working seamlessly together;
  - Defaults are more secure than in AWS (e.g. login tied to google accounts);
  - Has kubernetes --- used them and they work like a charm;
  - Their UI panel is super nice; Their API and CLI are nice as well;

  Cons:

  - Non-EU based company;
  - It's Google;


* http://ovh.com/

  Pros:

  - Cheap;
  - EU Based;
  - OpenStack provider;
  - Has (closed beta) kubernetes offering;

  Cons:

  - Varied performance;
  - Don't expect 100% uptime of everything, especially panel is glitchy;
  - If you are from Poland you can't pay via Credit Card (only PayPal
    and/or bank transfer)

* https://upcloud.com/

  Pros:

  - Cheap;
  - EU Based;

  Didn't use it;

* https://www.hetzner.com/?country=en

  Pros:

  - Cheap;
  - EU Based;

  Didn't use it;


* https://www.scaleway.com/

  Pros:

  - Cheap;
  - EU Based;
  - Have (alpha) k8s;

  Didn't use it;

* http://www.usshc.com/pricing/ or http://www.cyberbunker.com

  Two providers that promise to host your content "no matter what". Did not
  use them (but if you host things that --- while still being legal --- a
  are at risk of take-down) you might consider this or similar option. If you
  are wondering what might be risky --- consider woman rights issues or pages
  on child abuse by priests in Poland.

  It's a little bit pricy though.


Cloud providers verdict
***********************

Please note that this is mostly experience in organisations where we did not have
dedicated ops team (which would probably totally change cost-benefit ratio):

* For any kind of serious stuff I'd go for google cloud. K8s makes life simpler
  and Google Cloud is just too useful.
* For hobby stuff I'd probably go for hetzner, or ovh. They are cheap, and
  EU based.

Issue trackers
--------------

* https://kanboard.org/
* https://www.jetbrains.com/youtrack/

  I recommend for software projects. Way more usable than JIRA.

* Gitlab/Github issues

  More or less minimal board with issues. Good for small teams and simple
  projects.


Issue tracker verdict
*********************

If you can use built in tracker in your repository go for it, if not
use youtrack.

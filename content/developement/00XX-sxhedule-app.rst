Worker schedule maker, or how to extend legacy code
===================================================

:date: 2019-07-14
:tags: project,legacy-code,python,numpy
:slug: schedule-app
:status: draft

In this project I was extending legacy code of project that
optimized worker schedules. I did almost all of the technical
work.

The Project
-----------

This was an one-man project, I was given codebase in Python, which was
not exactly *old*, but well it had a lot of technical debt.

It contained:

* One outdated readme file, that told nothing about architecture;
* No comments (only commented-out code);
* Some tests, but (to my surprise) commented out;
* A lot of non-obvious matrix operations;

.. note::

    I really appreciate author/s of this "legacy version",
    it was delivered and the client was happy enough that they
    contracted two follow-up projects.

    So while code quality could have been better, whole project
    served it's usefull purpose.

Overall algorithm
-----------------

Project was doing employee schedule optimization using simulated annealing
technique, so basically it worked as follows:

Whole schedule was represented as a single large matrix, rows represented
employees, and columns time-slots. One meant that employee was working during
a given time-slot.

At each step:

0. Current temperature is calculated;
1. Random strategy is selected and used to modify matrix;
2. Then cost is calculated, if cost passes check (that depends on temperature)
   go to next step, if not roll back;
3. Schedule is validated if invalid roll back;
4. If we are at last iteration return current matrix and produce output;

Simulated annealing
-------------------

Simulated annealing probabilistic technique, that approximates global minima
of a problem, in a way that tries to minimize a chance of getting stuck in
shallow local minima.

Optimization algorithm has an extra meta-parameter, called ``temperature``,
and an acceptance function ``accept(T, old_cost, new_cost) -> bool``.
The lower the current temperature is the higher the chance is to accept
new solution with bigger cost. Accepting solutions with bigger cost
allows the algorithm to exit basins of local minima, to (hopefully) find
basin of global minima (or just basin of **good** local minima).

During the annealing process temperature falls, so the algorithm will be less
and less eager to accept worse solutions, so in the end we will be in *some*
minimum.

Project modules
---------------

Project *could* be roughly split in following parts:

DTO's
    That is classes that parsed JSON format and produced json response;

Worker Matrix
    This class provides the ground truths about current state of the
    schedule;

    Contains most of the indexing structures;

    Somewhat a God Object.

Cost Calculations
    Code that calculates the cost of current solutions;

Strategies
    Strategies that modify the Worker Matrix to optimize it;

Annealing Search
    Class that performs the actual simulated annealing;

Work log of changes
-------------------

1. Re-introduced all tests, added CI;
2. Formatted all code using ``black``;
3. Introduced proper frozen requirements;
4. Refactored all *logical modules* to be in separate *python packages*.
5. Reforormated all DTO's to:

   1. Use more understandable field names;
   2. Be mostly immutable;

6. Documented everything I found in all the modules;
7. Removed all dead code I found;
8. Actually added the functionality I needed to add;

   This was about half time of the whole project I'd guess.
9. Have the software tested by the client;

   1. There were 3 regressions introduced;
   2. There were 5 latent bugs fixed from previous version;
   3. There were 2 biggish issues due to requirements not
      clearly communicated;

Noteworthy changes I did (that might benefit readers)
-----------------------------------------------------

`List of general pointers on *how to handle legacy code* can be found in
a separate article. <{filename}./0028-how-to-handle-legacy-code.rst>`__.



Ethical note
------------

How to create password hashed in Linux ``/etc/shadow`` format (crypted password)
================================================================================

:date: 2015-07-15
:tags: linux
:summary:

   To (easily) create passwords crypted with ``crypt(3)``, just use
   ``mkpasswd`` program, on debian it is in ``whois`` package.

To (easily) create passwords crypted with ``crypt(3)``, just use
``mkpasswd`` program, on debian it is in ``whois`` package.

.. note::

   This observation might be obvious, but before I learned about this
   program I ended up creating my own implementation of it using python
   ``crypt`` module.

.. note::

  If you wonder why ``mkpasswd`` is in ``whois`` package,
  `here is the answer <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=116260>`__.
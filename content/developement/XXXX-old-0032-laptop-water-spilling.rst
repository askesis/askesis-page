Zalałem laptopa co robić jak żyć
================================

:date: 2017-04-09
:tags: laptop,spill
:lang: pl
:summary:

    Guest entry in Polish --- how to fix your laptop after spilling water on it.


(Note: this is guest entry in Polish)

Rozlewam wodę (herbatę) na laptopa średnio raz na pół roku, dodatkowo
komputer jest dla mnie głównym źródłem utrzymania więc utrzymanie go
na chodzie jest dość istotne.

Po pierwsze warto mieć komputer, który da się łatwo po zalaniu naprawić,
czyli coś co:

1. Da się rozłożyć;
2. Gwarancja zawiera ubezpieczenie od przypadkowych uszkodzeń,
   oraz masz klauzulę naprawy on-site (komputer jest naprawiany u
   Ciebie w domu, a nie musisz go wysyłać do serwisu);
3. Możesz próbować kupić komputer "spill proof", co nie wiele
   gwarantuje.

ThinkPady Lenowo są marką która spełnia te wymagania.

Instrukcja postępowania po zalaniu laptopa:

W okresie gwarancji:

0. Przytrzymujesz guzik power aż się komputer nie wyłączy.
1. Wyłączasz laptopa z prądu.
2. Wyjmujesz baterie.
3. Odwracasz rozłożonego laptopa żeby płyn wypłynął z klawiatury i nie
   penetrował dalej komputera
4. Dzwonisz na gwarancję.

Po okresie gwarancji:

0. Przytrzymujesz guzik power aż się komputer nie wyłączy.
1. Wyłączasz laptopa z prądu.
2. Wyjmujesz baterie.
3. Odwracasz rozłożonego laptopa żeby płyn wypłynął z klawiatury i nie
   penetrował dalej komputera
4. Czekasz z godzinę aż płyn wycieknie.
5. Rozkładasz laptopa na części i każdą wycierasz ręcznikiem papierowym.
6. Dajesz rozłożonemu czas żeby wysechł (ze 24h). Nie kładź niczego na
   żródle ciepła (kaloryfer)! Niech schnie w temparaturze pokojowej.
7. Sprawdzasz czy wyschło i składasz komuter.
8. Prosisz Latającego Potwora Spagetti o pomoc i włączasz.

Fun fact: zalania prawie nigdy nie przeżywa klawiatura, warto mieć zapasową
USB.


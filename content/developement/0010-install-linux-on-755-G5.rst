Install Linux on Hp EliteBook 755 G5
====================================

:date: 2018-09-16
:slug: hp-755-g5
:tags: hp,755 G5,elitebook,linux,debian
:summary:

Some time ago I bought Hp EliteBook 755 G5, installing linux on it wasn't best
experience I had, so here is what went wrong and what I did to fix it.

Issues I had when installing Linux on HP EliteBook 755 G5
---------------------------------------------------------

I had **weirdest errors** when installing/using Linux on this laptop
including (it's here just for reference and improved search visiblilty).

* Arch/Manjaro Linux (no GUI) --- freezes during install.
* Debian Stable (only vesa graphics card)
* Debian Testing (frozen during install; bear in mind that eventually --- see step 1 --- I'm using buster)
* Red Hat Enterprise Linux (almost working, but I got a lot of "Hardware Errors"
  in syslog). Also in about 50% cases system wouldn't boot (but then after restart
  it would).

If you are experiencing such issues it means that you have invalid bios installed,
and you need to downgrade it (see below for details).

Solution
--------

You'll need to **downgrade** your bios to version
``01.00.04 Rev.A 13.6 MB Jun 8, 2018``, you can download one from HP website.

Easiest way to install it is to download and run supplied exe on original windows.

I have no idea why this works,
`here is original suggestion <https://h30434.www3.hp.com/t5/Notebook-Operating-System-and-Recovery/Linux-installation-on-HP-EliteBook-735-G5-impossible/td-p/6791488>`__.

Debian installation procedure
-----------------------------

Here is what I did:

1. Downgrade bios;
2. Install Debian Buster (yes you need testing as recent kernel is needed);
3. Install ``firmware-amd-graphics`` package that contains amd card drivers;

Wifi driver
-----------

`See this bug for debian issue tracking
<https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=905867>`__.

Long story short: there is some issue with ``firmware-realtek`` package, and
proper module is not built, so I needed to build it from sources, via:

.. code-block:: bash

    git clone https://github.com/lwfinger/rtlwifi_new.git
    git checkout origin/extended -b extended
    cd rtlwifi_new
    make install
    modprobe -r rtl8822be
    modprobe -v rtl8822be

More specifically, commit: `a562bccf219f6842608125f6aee01d9da3b2dedd` worked.

Amd graphics drivers
--------------------

Everything seems to work OK (will write more when I get more tests).

I get error report in syslog from time to time, `see this bug for details <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=910272>`__


Useful Hints
------------

1. Don't delete windows, I guess you'll need it to upgrade bios/microcode.

   I just backed up whole disk image to nas server in my home using debian
   live edition and following command:

   .. code-block:: bash

        cat /dev/nvme0n1 | pigz | ssh my-nas 'cat > /data/file.img.gz'

   Tarball was about 40GB size.

   Probably you can use more fancy tools, but whatever works works.

2. Try a throwaway install, I wasted a lot of time when installer overwritten
   volume with random data for dm-crypt.
























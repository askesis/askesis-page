How to replace pelican ``index.html`` with restructured text
============================================================

:date: 2018-04-29
:slug: pelican-index-page
:summary:
    How to replace pelican ``index.html`` with page written in restructured
    text.

:tags: pelican, python, foo, bar, baz

While developing **this** blog I wanted to have ``index.html`` page that,
instead of showing list of articles (which will be shown on archives page
linked from main menu).

I found `following howto guide
<https://siongui.github.io/2016/02/19/pelican-generate-index-html-by-rst-or-md/>`__
but it involved writing custom jinja2 filter, which seemed to be some maintenance
burden (and according to my slogan [#]_, I need this blog to be maintenance free).

So here is simpler solution:

* Create ``page-content.html`` template that contains only contents of a page
  (that is: it's header contents and any footer you need). Usually just copy-paste
  contents of ``page.html``
* ``{% include "page-content.html" %}`` in ``page.html``.
* Create index page (set ``:slug: index``), and set it's status to hidden: ``:status: hidden``.
* In case of my template (copied from ``simple`` template built in pelican)
  ``category.html`` did inherit from ``index.html``, so I just copied
  ``index.html`` to ``category.html``.
* Replace ``index.html`` contents with:

  .. code-block:: jinja2

     {% for hidden_page in hidden_pages %}
        {% if hidden_page.slug == 'index' %}
            {% with page = hidden_page %}
                {% include "page-content.html" %}
            {% endwith %}
        {% endif %}
    {% else %}
        <strong>Please add hidden page (:status: hidden) with slug <i>'index'</i></strong>
    {% endfor %}


.. [#] Which can be: "Make it as simple as possible but not simpler".
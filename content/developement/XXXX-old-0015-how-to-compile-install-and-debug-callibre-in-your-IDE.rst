How to compile, install and debug Calibre in your favorite IDE
===============================================================

:date: 2014-08-13
:tags: python,calibre
:summary:

   How to compile and debug Calibre in your IDE

Calibre is a very nice e-book organizer, however development documentation
is, quite laconic.

Installing it for the first time turned out to be a major pain,
when I installed it for the first time, as I had to do some experimentation.

I have installed calibre to a virtual environment, which also is a pain. Nevertheless here is what to do.

1. Download calibre source

   .. code-block:: bash

      git clone https://github.com/kovidgoyal/calibre

2. Install all system dependencies (there quite many of them). For systems
   not based on Debian install them by hand.

   .. code-block:: bash

      sudo aptitude build-dep calibre

3. Create a virtual environment. You'll have to pass ``--system-site-packages``
   switch that allows virtualenv to use system python packages.

   .. code-block:: bash

     cd calibre
     virtualenv -p python2 --system-site-packages venv
     source venv/bin/activate

4. Try to launch ``setup.py``

   .. code-block:: bash

        python setup.py
        Traceback (most recent call last):
          (...)
          File "/tmp/calibre2/setup/build_environment.py", line 103, in get_sip_dir
          (...)
        EnvironmentError: Failed to find the location of the PyQt5 .sip files

   Calibre can't find system SIP files for PyQt5, you'll need to edit
   ``setup/build_environment.py`` find a line that contains:

   .. code-block:: python

        pyqt['pyqt_sip_dir'] = get_sip_dir(sys.prefix if iswindows else os.path.join(sys.prefix, 'share', 'sip'))

   and replace it with:

   .. code-block:: python

        pyqt['pyqt_sip_dir'] = get_sip_dir(sys.prefix if iswindows else os.path.join('/usr/', 'share', 'sip'))

   Just replace ``sys.prefix`` to ``/usr``, if you installed PyQt and other
   dependencies for calibre setup.py should work now

5. Launch ``python setup.py  bootstrap``. This command does some stuff I don't
   really understand, but it is necessary to build calibre.

6. Install Calibre in development mode: ``python setup.py develop``.

7. Now to debug Calibre you'll just need to launch ``venv/bin/calibre`` in
   debug mode in your favorite IDE.





SSH-tunnels
===========

:date: 2018-05-26
:slug: ssh-tunnels
:tags: python
:summary:

    Reference is here: https://unix.stackexchange.com/a/118650/5612

Every time I need to set up ssh tunnel I need to google command incantation.

Here is very useful reverence: https://unix.stackexchange.com/a/118650/5612
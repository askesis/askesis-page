How to extract images and tables from pdf-s
===========================================

:date: 2014-02-03
:tags: debian,pdf
:summary:
    HOWTO for extracting images and tables from pdf files

You'll need to use ``convert`` tool from the imagemagic suite.

.. code-block:: bash

    convert -density 450 -quality 100 file.pdf foo.png

And you'll get image for each page.

Since by default convert created image files with low resolution, after
too much googling I found that you need to fiddle with ``density``
and ``quality`` switches.

How to extract tables from pdf-s
--------------------------------

Extracting tables from pdf files is hard, as in pdf there are not tables,
just lines and letters.

I use `this tool <https://github.com/ashima/pdf-table-extract>`_,
it is able to extract most of tabular data and recovers structure very well.

Downsides are that it is painfuly slow (launches a process to extract each cell).


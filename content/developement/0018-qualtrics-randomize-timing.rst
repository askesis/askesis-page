How to have randomized survey with each question timed
======================================================

:date: 2019-04-15
:slug: qualtrics-random
:tags: qualtrics

Qualtrics is a enterprisy survey tool that is very popular in psychology research field.

So I needed to create a survey in Qualtrics, where:

1. Order of questions in a section will be randomized;
2. Each question will be timed;

To my suprise it's not obvious how to do it, `there are even unresolved
questions on a Stack Exchange site <https://psychology.stackexchange.com/q/19582/21787>`__.

So here is how to do it:

1. Each question should be paired with a timing question.
2. Each question pair (that is: "normal question" and timing question)
   should be put into a separate block.
3. Then you go to Survey Flow, and add these blocks to a randomizer element;
4. To provide intro and outro sections you might want create Group flow element;

In the end my survey looked like that:

.. figure:: {static}/images/0018/randomiser.png
    :width: 80%
    :alt: Randomiser with question blocks. Each block contains a question
          and a timing question.

    Randomiser with question blocks. Each block contains a question
    and a timing question.

.. figure:: {static}/images/0018/group.png
    :width: 80%
    :alt: Survey part with intro, outro and randomiser.

    Survey part with intro, outro and randomiser.


PS. If you need help some help with getting qualtrics working precisely like you
want it to work --- do get in touch with me at ``jacek@askesis.pl``.


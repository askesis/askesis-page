``pgdump`` command is sensitive to latency
==========================================

:date: 2017-04-09
:tags: postgresql
:summary:

    It turns out that ``pgdump`` command is sensitive to latency between ``pgdump``
    and ``postgresql`` database.

It turns out that ``pgdump`` command is sensitive to latency between ``pgdump``
and ``postgresql`` database.

Some time ago I was trying to download database from (literally)
other side od the world --- and I noticed that ``pgdump`` was just staling.
Download of very small database (couple of megabytes) took about an hour.

It turns out that the problem was **latency** between computer running
pgdump and the database itself, ``pgdump`` is doing a lot od requests that look
like that:

* Request some information about a relation (be it table, index, ...)
* Wait until you get an answer.

There is at least 7 round trips **per table**, so downloading empty table with
one second round-trip latency takes 7 seconds at least.

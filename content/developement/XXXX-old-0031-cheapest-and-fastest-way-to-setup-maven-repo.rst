Cheap and fast way to create your own Maven repository on S3
============================================================

:date: 2017-04-11
:tags: linux,java,maven,s3
:summary:

    Maven is de-facto standard build system for Java apps, it's a nice and mature piece of software.

    Here is how to host Maven repository on S3 without a server.

Maven is de-facto standard build system for Java apps, it's a nice and mature piece of software.

To meaningfully use it you need to host your own maven repository --- that allows you and
your teammates to download and release software artifacts you use. Most of these repositories
are written in Java, and not very light --- most probably you'd need to have a dedicated VPS,
which is OK --- unless you use this repository for hobby project that is in maintenance mode
and you touch it once a year.

Cheap and easy way to host Maven repositories is to use S3, there you pay only for storage, and
transfer, which is preatty cheap.

Simple guide is here: https://github.com/spring-projects/aws-maven.

.. note::

    Setting private maven repositories on S3 is less straight forward, if you
    really need how-to drop me an e-mail I'll try to update this note with howto.
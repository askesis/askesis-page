How to put math in pelican blog posts withoug relying on third-party CDN
========================================================================

:date: 2020-08-11
:tags: pelican
:slug: pelican-math

This blog is rendered using pelican, pelican is a python program that reads
files in restructured text (or in markdown) and produces a static site.

Recent versions of pelincan has a plugin that transparently handles math
directives, and renders them using MathJAX library. Hovewer it uses mathjax from
``cdnjs.cloudflare.com``, and I have decided that this blog will contain
resources only from domains I control.

Luckily it turned out that fixing this was easier than I thought:

1. Copy the ``es5`` directory from the `github mathjax repository <https://github.com/mathjax/MathJax/tree/master/es5>`__.
   to the static file folder **of your template**.
2. Add the following to your site config:

   .. code-block:: python

        MATH_JAX = {
            "source": '''"''' + SITEURL + '''/theme/mathjax-es5/tex-chtml-full.js"'''
        }

   I think that ``/theme/`` should be more along the lines of
   ``THEME_STATIC_DIR`` variable in jinja context, but I don't really care that
   much.
3. Sadly you'll need to redo it in ``publishconf.py`` as it uses different
   ``SITEURL``.
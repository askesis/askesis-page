Disabling Optimus on Debian and using only Nvidia Graphics card
===============================================================

:date: 2014-01-01
:slug: optimus-nvidia-disable
:tags: cuda,bumblebee,optimus,nvidia
:summary:
    Nvidia optimus somehow broke my CUDA drivers so here is
    how to disable it.

.. note::

    This is a post from 2014, please check if it still applies,
    at the time of writing bumblebee was relatively new.

I had some problems with bumblebee and optirun command --- it
just suddenly stopped working.

So here is how to switch your computer to use nvidia on debian,
and use nvidia proprietary drivers

1. Remove ``bumblebee``
2. Install Nvidia proprietary drives (https://wiki.debian.org/NvidiaGraphicsDrivers).

   .. note::

       Remember to configure ``xorg.conf`` precisely as in
       the instructions.

3. Restart. Go to bios and switch off integrated graphics card.

If you restart your computer before step 3, most probably your
X will not be usable.

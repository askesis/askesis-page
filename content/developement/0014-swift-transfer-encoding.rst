OpenStack Swift ``Transfer-Encoding`` problem
==============================================

:date: 2019-01-05
:slug: swift-tranfer-encoding
:tags: swift,http
:summary:
    OpenStack Swift ``Transfer-Encoding`` problem

Our OpenStack vendor did some upgrades to their infrastructure, which broke
upload to swift in a single place in our system.

Only trace in logs was::

    RESP BODY: b'Unsupported Transfer-Coding header value specified in Transfer-Encoding header'

So if you are poor soul that has similar problem here is what I found.

For some reason our provider decided to throw away ``PUT`` with
``Transfer-Encoding: chunked`` header.

Swift uses following heuristic to
determine whether to send ``Content-Length`` or ``Transfer-Encoding``:

If ``swiftclient.service.SwiftService.upload`` gets an

* upload that has a ``read`` method client treats it as a ``file-like``
  and proceeds to use ``Transfer-Encoding``;
* if it gets a path it reads size, and then either
  sends ``Content-Length`` for small files or uses ``Transfer-Encoding`` for
  files bigger than maximal object size (usually ``5GB``).

In my case I was uploading an unpacked zip archive, so I just cached zip entries
to a temporary file and uploaded that.

.. note::

    Reading swift client source makes me want to cry.

Modern vue.js
=============

:date: 2019-02-12
:slug: modern-vue
:tags: nodejs,vue1,vue.js,lexicon

I'm not a frontend developer, well not exclusively, and 90% of my projects
don't need any kind of heavy javascript component framework. You can get
*a long way* using plain HTML with some javascript sprinkled on top
(which is IMO much faster to write than doing full-blown component, and/or SPA).

Lately, I needed to write some non-trivial component, and here are my experiences.

Don't be afraid of writing vue.js *single file components*
----------------------------------------------------------

Vue is a very nice open-source JavaScript application framework. I chose it some time
ago as my framework of choice, as it looked simpler than ``angular`` and
``react``.

Single file components are --- just like the name suggests --- a method of creating
``vue.js`` components that contain in a single file: an HTML template, javascript
component code, and CSS styles.

``Vue.js`` documentation states:

.. pull-quote::

    With ``.vue`` components, we’re entering the realm of advanced
    JavaScript applications.

This sentence is true, however ``vue-cli`` is super nice, and manages to
hide most of the complexity.

If you know what ``nodejs`` is and have (even very fuzzy) understanding of
what javascript modules are, go for single file components and don't look back.

``vue-cli`` is a very nice project
----------------------------------

I was astonished by the quality of ``vue-cli``.

``vue-cli`` is a tool that lets you generate vue project template. Most of such tools
(not only in javascript but in python too) just generate scaffolding for you,
and then you are on your own. ``vue-cli`` on the other hand, hides all
the scaffolding inside ``@vue/cli-service`` dependency (which even has it's own
plugin system). Internal build system details are hidden, it also makes
upgrades much easier.

Giving such structure to the template is **great**, as when new
vue version comes out, you can just upgrade it, alongside **whole
required tooling**  [1]_ (well I hope it will end this way)

``vuex`` is almost always a good thing
--------------------------------------

`vuex <https://vuex.vuejs.org/>`__
is a tool that lets you streamline state management.

``vuex`` documentation says:

.. pull-quote::

    Although Vuex helps us deal with shared state management, it also comes
    with the cost of more concepts and boilerplate. It's a trade-off between
    short term and long term productivity.

    If you've never built a large-scale SPA and jump right into Vuex,
    it may feel verbose and daunting. That's perfectly normal -
    if your app is simple, you will most likely be fine without Vuex.

In my experience, I have never regretted using ``vuex``, while I regret
not using it in a couple of projects. I might be skewed, as I tend to
try not to use a component framework for simple interactions ;), so when
I use it, it means that components will be complex.

You (most probably) don't need any kind of requests library
-----------------------------------------------------------

When I started doing web development the best (and only!) way for doing
asynchronous requests in a webpage was `XMLHttpRequest <https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest>`__
which was so ugly nobody used it without some wrapper to make it sane.

All request libraries I found on ``npm`` were, hmm, heavyish (in terms of kb's
added), but mostly we don't need them now. There is beautiful, modern,
built-in, working on ~90% of browsers `document.fetch <https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API>`__
so if you don't care for IE, opera mini and Blackberry Browser [2]_
(according to `caniuse.com <https://caniuse.com/#search=fetch>`__) just use it.
And if you need to support them there are polyfills available.


Explanation of terms used in this post
--------------------------------------

``vue.js``
    Very nice open-source JavaScript application framework. Selling of such
    applications frameworks (other major are: ``angular`` and ``react``) is that
    they streamline keeping some state (usually represented as a JSON object
    and read from remote API) with HTML display.

``webpack``
    A build system for transforming and bundling JavaScript applications,
    it takes your project (containing TypeScript, JavaScript, resource files)
    and turns them to a couple of JavaScript bundles ready for install in
    browser.

``vuex``
    A "state management" library for vue. That is a library that makes state
    management easier.

``polyfill``
    In frontend development, polyfill is a code that implements a feature on
    a browser that doesn't implement it natively.

.. [1] This might not be **obvious** why such an approach is *a good thing*,
       my experience is that often scaffolding code from template projects
       doesn't age well (or rather: rots quite fast).

       While I'm 100% comfortable maintaining template code from e.g.
       ``cookiecutter-django`` project, maintaining webpack config files
       is a little out of my comfort zone.


.. [2] Obviously throwing away 15% if potential costumers might not be a best
       idea, on the other hand, all desktop browsers released in the last two
       years support fetch.
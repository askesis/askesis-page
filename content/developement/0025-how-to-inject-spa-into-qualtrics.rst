How to inject a Javascript SPA to qualtrics survey
==================================================

:date: 2019-06-23
:slug: qualtrics-spa
:tags: webpack,qualtrics,webdev,javascript

Sorry, just don't. It won't work, it will be painful. You can integrate
your Single Page App (SPA) using ``iframe`` (this has its own caveats though
namely resizing).

I got an error with ``webpackJsonp`` not having the proper type, I would guess
that ``Qualtrics`` used their own webpack code (this is sensible for their scale)
and somehow I got version mismatch, or just one can't have two webpack apps
applications on the same site (which is also sensible). Life is tough without
proper namespaces.

.. note::

    If you managed to avoid iframes do let me know! I'll happily update this post and link to your solution :)

If this sound like black magic (or just something you'd rather avoid doing),
do get in touch I might help you with whatever you need :). My address is ``jacek@askesis.pl``.
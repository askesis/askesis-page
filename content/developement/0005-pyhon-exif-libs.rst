List of python exif libraries
=============================

:date: 2018-06-29
:slug: python-exif
:tags: python
:summary:

    List of python exif libraries as of 2018

``Piexif``
    Simple pure-python library that reads and writes EXIF from jpeg and tiff files.

    Seems nice and has no native dependencies.

    https://pypi.org/project/piexif/

``Pillow``
    Swiss army knife library for image manipulation. Does not support modifying
    exif tags (as far as I know). Does not provide public api for exif getting
    tags.

    Is nice since if you are doing any image manipulation you probably use PIL.

    .. code-block:: python

         from PIL import Image

         image = Image.open(f)
         exif_data = image._getexif()

    https://pillow.readthedocs.io/en/5.1.x/

``py3exiv2``
   Standard library for manipulatinb exif tags seems to be: http://exiv2.org/.

   It has python wrapper here: `py3exiv2 <https://pypi.org/project/py3exiv2/>`__

``gexiv2``
    GOobject wrapper over ``exiv2`` which has native python bindings.

    I did not use this one due to lack of clear instruction on how to install it
    in virtualenv.

    https://wiki.gnome.org/Projects/gexiv2




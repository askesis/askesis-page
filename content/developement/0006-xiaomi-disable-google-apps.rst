How to disable google apps in Xiaomi phone
==========================================

:date: 2018-08-20
:slug: google-xiaomi
:tags: android,google,xiaomi
:summary:

    How to disable google applications in Xiaomi phones.

Due to their shady legal actions google forced pre-installation of google apps
on almost all android phones (`here are European Commision finding about this
<http://europa.eu/rapid/press-release_IP-18-4581_en.htm>`__).

Google apps also use shady dark-UI patterns to suck all your data (here I have
only anecdodal evidnce where I and my wife repeatadly uninentionally turned
tracking --- or "cloud sync" on).

So here is guide on how to disable them:

1. Install `Activity Launcher application <https://play.google.com/store/apps/details?id=de.szalkowski.activitylauncher>`__
   is' and application that allows you to launch any activity (more or less --- program)
   installed in your system.

   You'll use it to launch version of "Apps" Settings menu that allows you to disable
   google apps (normal version doesn't allow you to disable them).
2. Start Activity Launcher App.
3. Select "All activities" on the top (or "Wszystkie Aktywności" in , you should now see long list of installed
   applications.
4. Scroll down to Settings (or "Ustawienia" in Polish), and search for
   "Installed Apps" (or "Zainstalowane aplikacje" in Polish). There will be
   more than entries with this name, chose one that has subtitle ending with: "AllAplicationsActivity".
5. Tap on "Installed Apps" and now you can disable Google Maps, Gmail and so forth.


Note --- next phone I buy will probably have some fully free OS.
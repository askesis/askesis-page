Debugging issues with phantomjs and "attached pdfs" in Invoice ninja
====================================================================

:date: 2018-09-17
:slug: invoice-ninja-phatnom-js
:tags: php,invoice-ninja
:summary:

Invoice ninja is a nice tool for invoice generation (and payment managment). It allows you to send emails with attached
invoices to clients, unfortunately pdf generation process is crazy complex, involving at invoice ninja calling
itself couple of times, and nontrivial amount of javascript.

How to debug issues with feature:

1. Make sure that https is working properly, by using console tools.

   Https might work in browser and still be broken for some clients,
   check if ``wget https://invoice.ninja.example.org/test_headless`` downloads something.

   One such error scenario is when you don't upload intermediate certificates to your
   cert chain. Browser will download them, but other clients will not.

2. Make sure that host is resolvable from your VM you installed invoice ninja at
   (and if you use docker, check that everything works **from inside docker container**).




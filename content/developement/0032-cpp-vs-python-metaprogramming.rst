Metaprogramming in C++ vs in Python
===================================


:date: 2020-12-25
:tags: python, attrs, C++
:slug: python-vs-cpp-metaprogramming

In the previous posts I have shown you how to develop code that formats and
parses arbitrary data types to and from JSON in C++ and Python:

* `Here is the Python code <{filename}0030-python-metaprogramming.rst>`__
* `Here is the C++ code <{filename}0031-cpp-metaprogramming.rst>`__
* `And here is the repository containing both implementations <https://gitlab.com/jbzdak/cpp-metaprogramming-blog-post>`__

To my surprise you C++ implementation wasn't that much more complex than Python
one, it is (to my greater surprise) also slightly more robust against future.
In Python I needed to access private (and unmentioned in the documentation:
``__args__`` and ``__origin__`` attributes of the typing annotation, and my
experience with ``typing`` module is that it **does change**, so while Python
code is brittle and will require changes once new Python is released, C++
version should be future proof.


Performance comparison
----------------------

C++ version is much faster than Python one. I have benchmarked both formatting
and parsing json, both benchmarks did the same thing:

1. Generate 10 different orders, and store them to a newline separated json
   file 500 000 times.
2. Parse the aforementioned file checking how many times particular order occurs.

Performance for C++:

1. Compile time: 6sec;
2. Formatting runtime: 13sec (86mb/sec);
3. Parsing runtime: 12sec;

Performance for Python:

1. Formatting: 113sec (10mb/sec);
2. Parsing: 164sec;



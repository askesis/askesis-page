SSH keepalive
=============

:date: 2019-05-13
:slug: ssh-keepalive
:tags: ssh,commands,trivia

I was doing some remote work on a very remote system, and my ssh was keeping
freezing, just as if something in the middle, would break TCP connection, but
didn't bother with ``FIN`` packets.

I guessed that this might be a problem with having too big keepalive,
so to change keepalive on a single SSH connection, just use:
``ssh -o "ServerAliveInterval XXX"``, where ``XXX``` is keepalive in seconds.

Mosh (secure) shell
-------------------

`Daniel <https://www.daniel.priv.no/>`__ suggested that I try *mosh*, which is
as ssh alternative that works on UDP, allows client roaming, and probably is
much more responsive.

Feel free to try it!

I (sadly) couldn't try it since:

* I routinely tunnel to web services via gateway boxes, due to variety of reasons
  (sometimes it's lack of public IP's, sometimes it's just that I feel good when
  I can enable one less port on the VM firewall).

  And ``mosh`` does not support ``ProxyCommand`` option from SSH.

* I routinely disable all unnecessary ports on firewall, and I'd rather not
  enable them.

.. note::

    I'm well aware that this is mostly an *aesthetic* choice rather than security
    one: as well if there is zero day in SSH my services would probably be as damaged
    as everybody else's (or maybe they would be safer as attacker would probably
    first scan 22 of every service they dislike --- and this wouldn't work ---
    and then stan port 22 of every IP --- which still wouldn't work).  However
    there are probably other endpoints to these systems.


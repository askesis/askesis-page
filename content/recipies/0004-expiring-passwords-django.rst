Expiring user passwords in Django
=================================

:date: 2019-08-08
:tags: python,django
:slug: expiring-passwords


I needed to add auto-expiry feature for user passwords in a Django project. It turned out to
be super simple.

Logic is: when expiry occurs user is redirected to a password change page.

.. note::

    I'm well aware that recent NIST recommendations discourage this practice,
    and I also think that monthly password rotation is moronic idea.

    However it is mandated by law in Poland for certain usages.

You can easily implement this as a middleware, which works like that:

.. code-block:: python

    class PasswordMiddleware:

        def __init__(self, get_response):
            self.get_response = get_response

        def __call__(self, request):

            user = request.user

            password_change_path = reverse("password_change")

            if user.is_authenticated and password_expired(user):
                if request.path != password_change_path:
                    return redirect(password_change_path)

            return self.get_response(request)

And you need to add this to middleware directly **after** ``AuthenticationMiddleware``.



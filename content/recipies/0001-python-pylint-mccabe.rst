Enable cyclomatic complexity check in ``pylint``
================================================

:date: 2019-07-14
:tags: python,pylint,mccabe,cyclomatic complexity
:slug: python-pylint-mccabe

Cyclomatic complexity is a quantitive measure of
program (procedure) complexity.

You can measure it in ``pylint`` and fail functions
that have complexity over maximal value.

I tried duck-ducking it but found some old answers, so here is how to enable it.

1. It is installed by default on recent ``pylint``, you need to tweak
   ``pylintrc``;
2. You need to add following plugin::

        load-plugins=pylint.extensions.mccabe

3. Add max complexity to the ``Design`` section::

    [DESIGN]
    max-complexity=10


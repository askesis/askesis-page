Literate testing in ``sphinx``
==============================


:date: 2019-07-14
:tags: python,sphinx
:slug: literate-testing

In one of my current projects, we **might** need to write some kind of
"reports" that describe and "prove" our fraud detectors work.

Such reports might not fit ``jupyter notebook`` as we might need some more
control on the layout.

On the other hand it would be nice to have everything tested.

Hopefully `sphinx <http://www.sphinx-doc.org/>`__ contains nice tools for
testing code in documentation, to enable them just:

1. Enable ``'sphinx.ext.doctest'`` plugin by adding it to the ``extensions``
   list in the ``conf.py`` file::

    extensions = [
        'sphinx.ext.autodoc',
        'sphinx.ext.doctest',
        'sphinx.ext.intersphinx',
        'sphinx.ext.coverage',
        'sphinx.ext.mathjax',
        'sphinx.ext.viewcode',
    ]

2. Then write some `doctests <https://docs.python.org/3/library/doctest.html>`__ like that::

        .. doctest::

            >>> def addition(left, right):
            ...      return left + right

            >>> addition(1, 3) == 4
            True
            >>> addition(1, 1)
            2

3. Then run it using ``make doctest`` and it will run all code and raise error
   if doctests won't work.


Online Educa Berlin notes
=========================

:date: 2017-12-17
:tags: oeb
:summary:

    OEB (former: Online Educa Berlin) is a major ed-tech conference. Here are my notes from
    2017 edition of said conference

.. warning::

    These are my own personal notes. They were **not authorized whatsoever**
    and most probably are both **factually inaccurate** and **slightly different** from
    what speakers said.


Day 1: Summit: Strategy under Uncertainty: Futureproofing Higher Education
##########################################################################

Australia remote higher education
---------------------------------

KAY LIPSON: Online Education Services

* 15% of Australian students are learning online. These are MS and Ba courses!
* Remote learning **is not competing with campus** it is entirely different demographics

  * 74% of remote students are female
  * 80% do degree part time
  * 94% is over 25 years old

* This expanded market for AU universities.
* Universities are having bad time scaling their remote learning. It might be good
  to outsource it to dedicated institution. (note: obviously speaker was
  selling such services)
* Online Education Services provide full support for this, from administration,
  enrollment to examination.

Minerva Schols KGI
------------------

Ben Nelson: Minerva, USA

* Basically political class sucks.
* This is a failure of higher education that dropped the idea of teaching
  virtue/character (in greek it would be "arete").
* "General Knowledge" subjects are treated like unwanted kids in technical
  schools. JB: In poland this is totally true.
* Higher education treches only disciplinary knowledge.
* People are bad at "transfer" that is transferring knowledge from one discopline
  to another. You might be physics proffessor with very string critical thinking
  in reactor physics, and yet when you apply this to eg. personal life or politics
  he might throw this out of the window.
* Minerva Schools tries to bridge the gap --- and teach general problem solving
  skills (with some engineering background). Program is also kind-of affordable
  totalling about 20k$ per 4 year programme.
* They say they get very good reaults and Will Change The World^TM


QA session
----------

How to scale remote education

1. Create small teams for students, of about 20~25 people.
2. Enforce that each of them is active.
3. Have proffesor handle 20 such teams.
4. You need to pay T/A forgrading
5. Minerva is trying to scale their process for remote learning, where
   inter-student communication enforces activity and lessens burden on the
   professor.


How to cut administrative costs while **improving** quality of support for students

*Answer is from Kay*

1. Change oulook form "We are office of the dean" to "We are customer support"
2. Often students take use time of faculty for administrative tasks. This is
   not efficient, as faculty often can't help them easily.
3. They do some simple counseling and or simple instruction questions (like
   where to find materials for the excercise)
4. This team can also get extensions etc.

.. note::

    I see a reason in moving these duties from the faculty to, well, anyone else.

How to change institutions

* Educational institutions have been here for last thousand year (there are
  actual institutions that are almost thousand years old!)
* They are resilient to change.

There were two answers to this problem:

* You need to start from the **very top**, probably provost. If not change will
  be **slow**
* You need to have acceptance for the change from the professors.


Ari Jónsson Reykjavik University, Iceland
-----------------------------------------

* We are under III industrial revolution
* AI Revolution
* Universities will need to change

David Lenihan Ponce Medical School, USA
---------------------------------------

* Medical schools do not teach soft skills.
* Theses skills are very important.
* Medical admission system doesn't take cultural/soft skills into account.
* But they have oral exam that requires you to know Shakespeare. . .
* Cultural skills are important. E.g. for example you dont touch white catholic
  males unless neccessary, while latino americans usually hug on welcome.
* We need more doctors of color.
* Medicine education can be standardized and moved online to provide more accessible
  teaching.

.. note::

    David suggested that essentially "all people are the same" so medical teaching
    can be standardized. This is true for the most part, but there are
    statistically siginificant differences between cultures and ethnical groups
    for various things (lactose intolerance comes to mind) this needs to be somehow
    handled.

Lauren Herckis Simon Initiative, Carnegie Mellon University, USA
----------------------------------------------------------------

* There are many good remote teaching tools in Carnegie Melon.
* Some of tese tools are not used systmeatically.
* Speaker is an anthropologist so she wanted to find out why this happens.

Thesis is:

* Faculty does not know **how to teach** and **how to measure outcomes**
* They do not get instructed.
* Some of them don't know that **teaching is a skill in itself** (most of them
  think that teaching skills somehow they **enchance their discipline skills**
  when teaching).
* Most of faculty **wants and likes to teach**.

Larry Cooperman University of California, Irvine, USA
-----------------------------------------------------

* Moocs were supposed to disrupt and destroy education.
* Yet here we are here.
* Universities are not distupted.

Q/A
---

Why MOOCS were not distruptors?

* This is according to the innovation theory: usually first iteration of
  new technology is worse than latest iteration of old technology.
* To be fair MOOCS are not very state-of-the-art. They are just videos + quizzes.
* While universities stand, change might be very rapid and come from government,
  as soon as other institutions demonstrate they can provide better education
  at fraction of the cost, change might be very rapid.

MOOCS in third world.

* Moocs are great help there!
* Digital colonialism is the problem --- we need to enable refugees to procude
  their own content in their own languages.

Real battle between Universities and Moocs will be about:

* Credit system, awarding diplommas
* **Maintaining education as public good**




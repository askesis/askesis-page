Jak ćwiczyć na diecie redukcyjnej
=================================

:date: 2018-05-06
:tags: exercise
:slug: excercise-weight-loss
:lang: pl
:summary:

    Ćwiczenie na diecie redukującej masę (czyli niskokalorycznej) jest trudne, ala
    możliwe tylko wymaga dokładnej kalibracji.


.. note::

    Nie jestem specjalistą ani dietetykiem, nie znam się na tym na czym piszę w
    tym wypadku. Zachęcam do czytania tego z dystansem, a jeśli chcesz schudnąć,
    znajdź sobie dietetyka.

Ćwiczenie na diecie redukującej masę (czyli niskokalorycznej) jest trudne, ala
możliwe tylko wymaga dokładnej kalibracji.

Kilka rzeczy które wiem

* **Nie jest prawdą** że jeśli spalisz N kalorii tyle samo możesz potem zjeść.

  Jest tak z kilku powodów:

  * Zdaje się że tak po prostu działa nasz organizm: duże wydatki energii
    uruchamiają adaptacje polegające na oszczędzaniu jej na przyszłość. [*]_
  * Ja osobiście notorycznie nie ufam estymacjom "ilości spalonych kalorii";

* **Nie powinno się jeść słodyczy** przed treningiem. Działa to tak: jesz słodycze
  podnosi się poziom cukru we krwi, trzustka wyrzuca insulinę która powoduje
  znaczny spadek owego poziomu we krwi, dochodzą ćwiczenia które go jeszcze
  obniżają. Więc ćwiczysz w dole cukrowym, który ani nie jest przyjemny, ani
  zdrowy, a w ekstremalnym przypadku możesz stracić przytomność.

* **Trudno jest zgubić kilogramy samymi ćwiczeniami**. Serio.
  Opakowanie czekolady Milka ma 600kcal (w 100gr). Ja potrafiłem swojego czasu
  wciągnąć 300gr na raz. To jest 1800kcal, czyli dobre trzy godziny na rowerze
  na szosie (dodatkowo pamiętajcie że wysiłek może powodować wejście organizmu
  w stan oszczędzania energii).

* **Zawsze miej w plecaku jakiegoś batonika energetycznego**. Bo czasem zjazd
  cukru się trafi, problem jest jak trafi się 20km od domu i 10 od sklepu.

* **Jeśli chcesz wzrostu masy mięśniowej** [*]_ to po wysiłku warto zjeść coś 
  co zawiera wysoko-przyswajalne białka (czyli w praktyce: odżywkę białkową
  --- ogólnie jest mało *naturalnych* produktów które mają mało kalorii i dużo
  białek. )

* **Ćwiczenia powinny trwać 30-60min** jeśli mają spalać tłuszcz --- mięśnie
  mają rezerwy energii na przynajmniej 15minut wysiłku --- więc pierwsze 15
  minut ćwiczeń w ogóle nie rusza rezerw tłuszczu.

* **Warto ćwiczyć regularnie** czyli dwa trzy razy w tygodniu, a najlepiej co
  dwa dni. Mój dietetyk tłumaczył, że ćwiczenia trochę podkręcają metabolizm
  więc jak ćwiczysz często to dodatkowo więcej kalorii spalasz szybszym
  metabolizmem.


.. [*] `Artykuł źródłowy <https://www.vox.com/2016/4/28/11518804/weight-loss-exercise-myth-burn-calories>`__,
       (`kopia web archive <https://web.archive.org/web/20180430135711/https://www.vox.com/2016/4/28/11518804/weight-loss-exercise-myth-burn-calories>`__.

   Cytat:

   | Dugas calls this phenomenon "part of a survival mechanism": The body could
   | be conserving energy to try to hang on to stored fat for future energy needs.
   | Again, researchers don't yet know why this happens or how long the effects persist in people.
   |
   | "We know with confidence that some metabolic adaptions occur under some circumstances,"
   | said David Allison, "and we know with confidence some behavioral compensations occur under
   | some circumstances. We don't know how much compensation occurs, under which circumstances,
   | and for whom."


.. [*] Masa mięśniowa nie jest celem sama w sobie. Przy chudnięciu ma tą przewagę
   że podnosi bazowy metabolizm (tj. utrzymanie kilograma mięśnia zużywa więcej
   energii niż kilograma tłuszczu).

Jak ja **teraz** ćwiczę
-----------------------

1. Godzinę przed treningiem jem większy posiłek który zawiera dodatkowo
   100-200kcal do spalenia.

   Te dodatkowe kalorie są w postaci makaronu (z mąki razowej), który jest
   gotowany *al dente* (makaron *al dente* --- czyli gotowany tak żeby był lekko twardy --- ma niższy
   indeks glikemiczny niż makaron trochę rozgotowany; tak samo makaron razowy ma
   niższy indeks glikemiczny niż pszenny) [*]_ .

2. Trening jest ustawiony tak żeby był po obiedzie.
3. Zawsze mam przy sobie na trasie batonik białkowy.
4. Teraz jeden trening to godzina roweru i 500-600kcal.
5. Po treningu wciągam odżywkę białkową i normalny posiłek.
6. Ćwiczę jakoś dwa-trzy razy w tygodniu.

.. [*] Indeks glikemiczny obrazuje to (z grubsza) jak szybko po zjedzeniu danej rzeczy
   podnosi się poziom cukru (i jak szybko potem spada). Przed treningiem lepiej jest
   zjeść rzeczy z niższym IG bo te "starczą na cały trening" i nie będzie skoków
   cukru jak po słodyczach.


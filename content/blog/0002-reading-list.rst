2019 Reading list
=================

:date: 2018-12-29
:tags: reading
:slug: 2019-reading-list
:lang: en
:summary:

    My reading list from 2019 (and late 2018)


My reading list from 2019 (and late 2018)

1. Haruki Murakami "Killing Commendatore"

   Typical Murakami.

   Overall very pleasant read.

2. Ursula K. Le Guin: "Gifts" w "Always Coming Home"

   Typical UKL (very good!).

   PS. "Always coming home" was published in Poland as an anthology
   containing, "Gifts", "Voices", "Powers", "Always Coming Home" and
   "Changing planes";

3. Ursula K. Le Guin: "Changing Planes" w "Always Coming Home"

   Atypical UKL (still very good). Contains descriptions of different "planes",
   my favourite plane is one where people gradually talk less when they get older,
   and go totally silent after puberty.

   This book is also has some anti-capitalistic motifs.

   Overall low stress due to episodic nature, contains some violence,
   some bloody history of particular planes.

4.  "The Fifth Season" N. K. Jemisin
5.  "The Obelisk Gate" N. K. Jemisin
6.  "The Stone Sky" N. K. Jemisin

    Very nice sci-fi/fantasy series, that breaks with most of common fantasy
    tropes.

    **Content Warning**: This is book is heavy, sometimes sad, and contains
    accurate descriptions of various hardships. It literally starts with
    someone starting an armageddon, which will kill all humanity (and most
    certainly destroy current civilisation).

    High stress books, that are totally not relaxing.

7. "Dawn" Octavia E. Butler
8. "Adulthood Rites" Octavia E. Butler
9. "Imago" Octavia E. Butler

    Very good sci-fi series.

   **Content Warning**: Can contain triggers concerning, consent,
   coercion, psychological violence, drugging.

   High stress books, that are totally not relaxing.

10. "The long way to small angry planet" Becky Chambers
11. "A closed and common orbit" Becky Chambers
12. "Record of a spaceborn few" Becky Chambers

    Very nice sci-fi series.

    Very relaxing read, overally about *growing up*, *becoming well adjusted*
    and *fighting own past traumas*, but well things mostly end well
    for everybody.

    **Total fluff**

13. "Trail of Lightning" Rebecca Roanhorse

    Urban fantasy in post-apocalyptic world where Navajo beliefs are true
    (kinda, sorta like Shadowrun). Not a nice read though, consecutive
    chapters were barely stitched together.

    **Content warning**: Contains a lot of violence (very graphic and personal!)
    some past recollections of psychological violence, and deals with
    bad relationship trauma.

14. "The Rise and Fall of D.O.D.O." Neal Stephenson

    Mediocre Stephenson (still decent book though).

    Fast read. Low stress. No specific content warnings (some violence, but
    kinda, sorta, unreal)

15. "Seveneves" Neal Stephenson

    Very good sci-fi --- great Stephenson. Very hard sci-fi, with very accurate
    orbital mechanics, and sensible protocols of handling nuclear contamination
    in space.

    **Content warning**: Moderate stress. Contains descriptions of various
    hardships in space, a lot of characters will die (some in unpleasant ways!).

16. "Fluff" Natalia Osińska

    Very nice coming-of-age young adult fiction about relationship with
    neuroatypical person.

    Very nice, very low stress.

    Sorry, this appears to be only in Polish.

    **Content warning**: Descriptions of abusive parenting.

17. "Weapons of Math Destruction" Cathy O'Neil

    Very nice book about risks of using data science, showing how data science
    might make choices based on wealth and race.

18. "Temper" Nicky Drayden

    Urban fantasy in a nice fresh world.

    Tropes:

    * Sibling relations;
    * Injustice and systematic discrimination;
    * Religious / Secular tensions;
    * Nature of virtue;


19. "The Raven Tower" Anne Leckie

    Fantasy in an atypical world.

    Highly recommended.

20. "A Memory Called Empire" Arkady Martine

    Very nice political space-opera.

    Highly recommended.
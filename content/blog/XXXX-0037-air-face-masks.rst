Maski przeciwpyłowe
===================


:date: 2017-12-10
:tags: oeb
:lang: pl
:summary:

    O This is another guest entry in Polish.

    Its about face masks to battle bad air quality (that is approaching Beijing
    smog levels) in Poland.

    Przez długi czas byłem sceptyczny co do masek. Poniekąd ciągle jestem.

    Po rowerowych wyjazdach zaczął łapać mnie suchy kaszel, który przerodził się w tygodniowy
    stan podgorączkowy. Ponieważ raczej nie było to przeziębienie (bo raczej nie mam
    od kogo się zarazić), stwierdziłem że to pewnie smog.

Dlaczego maski
--------------

Przez długi czas byłem sceptyczny co do masek. Poniekąd ciągle jestem.

W ramach podnoszenia zdrowia zacząłem jeździć na rowerze, jak każdy rozsądny człowiek
kupiłem nowy rower tuż po końcu sezonu z dobrą zniżką. No i zacząłem jeździć.

Staram sie jeżdzić możliwie kardio (co jest pewną zmianą) --- czyli jeżdzę z
prędkością przy której puls jest rzędu 170.

Po wyjazdach zaczął łapać mnie suchy kaszel, który przerodził się w tygodniowy
stan podgorączkowy. Ponieważ raczej nie było to przeziębienie (bo raczej nie mam
od kogo się zarazić), stwierdziłem że to pewnie smog.

Postanowiłem więc kupić maskę.

Jakie maski
-----------

Badania naukowe (które czytałem dawno) mówią takie rzeczy:

1. Jeśli chodzi o pyły zawieszone, w zasadzie wszystko Cię jakoś chroni, nałóż 
   apaszkę na twarz też pomoże, maseczka chirurgiczna blokuje 70% pyłów
   zawieszonych.
2. Zanieczyszczenia gazowe nie są już takie proste.

Inną sprawą jest to, że nie ma norm działania masek "miejskich", tj. to co kupujesz
w sklepie nie musi być badane, i tylko od dobrej woli producenta zależy czy
zrobi produkt dobry czy zły. Ponieważ klientowi trudno samodzielnie jest zweryfikować
jakość maski tego typu rynek z reguły pełęn jest produktów złej jakości.

Są jednak maski, które objęte są normami i ceryfikowane --- są to maski BPH ---
czyli takie maski które zakładają ludzie pracujący np. w lakierniach, tartakach
itp.

Możecie wybrać się do najbliższego sklepu BPH i zobaczyć jakie są opcje,
podejrzewam że cokolwiek co weźmiecie z półki będzie lepsze niż maski "miejskie".

Jaką maskę wybrałem
-------------------

Ja sobie kupiłem maske 7502 firmy 3M (takie same maski w innym rozmiarze to
7501 i 7503), wraz z filtrami 5N11 i 6001cn, znajomy sobie kupił taką samą 
maskę z filtrem 6035 (mój zestaw zawiera też pochłaniacz gazów i
par organicznych).

Teoretycznie filtry powinienem wymienić po 30 godzinach użytkowania albo po
miesiącu, zakładam, że na jednym zestawie dam radę przez całą zimę przejeździć 
(bo na razie zanieczyszczenia są mniejsze niż w lakierni).

Wyniki testów maski
-------------------

Wyniki testów:

1. Oddycha się zupełnie swobodnie;
2. Maska po treningu jest wilgotna od środka, jest to trochę uciążliwe w trakcie;
3. Nie czuć swądu spalenizny (sprawdziłem w paru miejscach --- bez maski czuć);
4. Nie ma kaszlu po treningu.

W sumie polecam.

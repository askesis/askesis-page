Lista książek z 2019
====================

:date: 2018-12-29
:tags: reading
:slug: 2019-reading-list
:lang: pl
:summary:

    Lista książek z końcówki 2018 i 2019 roku.


Lista książek które (prze)czytałem w 2019r (i w grudniu 2018).

1. Haruki Murakami "Śmierć Komandora /1"
2. Haruki Murakami "Śmierć Komandora /2"

   Typowy Murakami.

3. Ursula K. Le Guin: "Dary" w "Wracając wciąż do domu"

   Technicznie to nie **cała książka** ;) ale oddzielna powieść zbita razem
   z innymi powieściami z tego samego cyklu.

3. Ursula K. Le Guin: "Międzylądowania" w "Wracając wciąż do domu"


   Nietypowa UKL (bardzo dobra!). Zawiera opisy różnych światów (oraz metody
   podróży między nimi).

   Mój ulubiony z opisanych światów to ten w którym ludzie tracą chęć do mówienia
   wraz ze starzeniem się a w trakcie dojrzewania całkowicie milkną.

   Ma trochę antykapitalistycznych motywów.

   Nisko stresowa książka, zawiera opisy przemocy.

4.  "The Fifth Season" N. K. Jemisin
5.  "The Obelisk Gate" N. K. Jemisin
6.  "The Stone Sky" N. K. Jemisin

    Bardzo ciekawa seria fantasy/sci-fi, zrywająca ze wszystkimi konwencjami
    fantasy.

    **Uwaga**: nie jest to lekka i przyjemna książka, pierwsza powieść zaczyna się
    od literalnego armagedonu, który zgodnie ze wszystkimi znakami na ziemi
    wybije ludzkość (a już na pewno zaora cywilizację).

7. "Dawn" Octavia E. Butler
8. "Adulthood Rites" Octavia E. Butler
9. "Imago" Octavia E. Butler

    Bardzo ciekawa seria sci-fi.

    **Uwaga**: czytana współcześnie dotyka problemów
    consent (zgody na seks), gwałtów itp. ogólnie może triggerować.
    Nie jest to relaksująca lektura.

10. "The long way to small angry planet" Becky Chambers
11. "A closed and common orbit" Becky Chambers
12. "Record of a spaceborn few" Becky Chambers

    Bardzo fajna seria sci-fi. Ogólnie relaksująca lektura, w której nikomu
    nic złego się nie dzieje (znaczy motywem jest *zwalczanie traum* i
    *przystosowywanie się do życia*).

    Ogólnie pełen fluff.

13. "Trail of Lightning" Rebecca Roanhorse

    Urban fantasy, osadzone w świecie wierzeń Indian Navajo. Źle bawiłem się 
    przy tej książce --- poszczególne sceny nie kleiły się ze sobą kompletnie.

    **Content warning**: Opisy przemocy (bardzo osobistej!) przedstawione
    w wyjątkowo graficzny sposób. Przemoc psychiczna. Wychodzenie ze złych
    związków.

14. "The Rise and Fall of D.O.D.O." Neal Stephenson

    Bardzo przeciętna książka jak na Stephensona (co, i tak, znaczy że jest
    bardzo porządną lekturą sci-fi/fantasy).

    Bardzo lekka lektura. Zawiera przemoc (ale dla mnie dla mnie dość 
    odrealnioną).

15. "7EW" Neal Stephenson

    Dla odmiany --- świetna książka Stephensona. Dość twarde sci-fi, w którym
    główną rolę gra mechanika orbitalna (znów dość dokładnie opisana).

    Dodatkowo zawiera również ciekawy opis protokołów postępowania ze skażeniem
    jądrowym na robicie.

    **Uwaga**: Nie jest to lekka lektura, dość dużo bohaterów umrze często mało
    przyjemną śmiercią.

16. "Fluff" Natalia Osińska

    Bardzo fajna książka. Opisuje relację z osobą w spektrum autyzmu.

    Mało stresująca i zupełnie nie męcząca.

    **Uwaga**: Opisy bardzo przemocowych rodziców.

17. "Weapons of Math Destruction" Cathy O'Neil

    Dobrze opisana i uargumentowana książka pokazująca ryzyka mało świadomego
    tworzenia i stosowania modeli machine learningowych (które potrafią
    dziedziczyć rasistowskie uprzedzenia ich twórców).


18. "Temper" Nicky Drayden

    Urban fantasy w dość lekkim świeżym świecie;

    Motywy:

    * Relacje rodzinne;
    * Niesprawiedliwość i systemowa dyskryminacja;
    * Napięcia między religią a światem nauki;
    * Natura cnoty;

19. "The Raven Tower" Anne Leckie

    Fantasy w nietypowym świecie. Polecam.

20. "A Memory Called Empire" Arkady Martine

    Bardzo fajna Space Opera. Dużo motywów politycznych.

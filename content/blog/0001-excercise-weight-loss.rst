How to exercise on weight reduction diet
=========================================

:date: 2018-05-06
:tags: exercise,
:slug: excercise-weight-loss
:summary:

        Exercise on a weight reduction diet is hard, but doable. It requires careful calibration.

.. note::

    I'm not a specialist. If you want to do serious weight loss, find a
    dietitian.

Exercise on a weight reduction diet is hard, but doable.
It requires calibration.

Couple of facts
---------------

* **Following is NOT true:** If you burn N calories during exercise you can
  eat as much afterwards.

  First of all --- human body just works like that: big energy expenditures
  seem to kicks start adaptations that conserve energy [*]_.

* **Don't eat sweets before workout**. Here is why: sweets increase blood glucose
  levels, which induces pancreas to eject insulin into bloodstream. Insulin
  reduces glucose levels, if this reduction overlaps with reduction of glucose
  levels due to exercise your glucose level might be too low (which is not fun).

  Also in extreme cases you might faint.

* **Losing weight with training alone is hard**. Seriously.

  Bar of chocolate is 600kcal, an hour of intensive workout (for me).

* **Always have some chocolate/protein bar during workout**. Realising that
  you are hungry and 20km from home is not fun.

* **If you want your muscle mass to grow** [*]_ eat some easily digestible
  proteins after workout. Which means some protein supplements --- there are
  not much easily digestible, low calorie, protein sources in the wild.

* **If you aim for burning fat** workout should last 30-60 minutes. Muscle have
  an energy reserve for about 15 minutes, so you actually start to burn fat after
  thus 15 minute period.

  It's better to have lower intensity workout for one hour than very intensive
  15min one.

* **Regular exercise is good**, it should increase metabolic rate somewhat.

How do I exercise **now**
-------------------------

1. Hour before excercise I eat bigger lunch. Extra 200kcal --- usually wholemeal
   pasta cooked *al dente*.

   Wholemeal pasta has lower glycemic index than normal pasta, also pasta cooked
   *al dente* has lower glycemic index than normal pasta. [*]_ .
2. During the workout I always have some protein bar.
3. Usually single workout lasts about an hour and burns 600kcal.
4. After the workout I eat some extra protein (supplement)
5. Usually I workout twice or thrice a week.


.. [*] `Source <https://www.vox.com/2016/4/28/11518804/weight-loss-exercise-myth-burn-calories>`__,
   (`web archive <https://web.archive.org/web/20180430135711/https://www.vox.com/2016/4/28/11518804/weight-loss-exercise-myth-burn-calories>`__).

   Quote:

   | Dugas calls this phenomenon "part of a survival mechanism": The body could
   | be conserving energy to try to hang on to stored fat for future energy needs.
   | Again, researchers don't yet know why this happens or how long the effects persist in people.
   |
   | "We know with confidence that some metabolic adaptions occur under some circumstances,"
   | said David Allison, "and we know with confidence some behavioral compensations occur under
   | some circumstances. We don't know how much compensation occurs, under which circumstances,
   | and for whom."


.. [*] Muscle mass is not a goal in itself. When doing weight reduction it is good
   since it increases base metabolic rate --- keeping a kg of muscle tissue uses
   a lot more energy than 1 kg of fat tissue.

.. [*] Glycemic index is (roughly) how fast glucose levels rise after eating
   certain product (and how fast it falls afterwards). Before workout you want
   to eat things that release energy slowly so glucose levels don't crash during
   the workout.

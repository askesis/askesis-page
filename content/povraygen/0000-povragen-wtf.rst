What is povraygen
=================

:date: 2020-09-30
:tags: povraygen,python,povray,hobby

Povraygen is a hobby project that aims to randomly
generate realistically looking fantastic landscapes.

Currently it works as follows:

* Bunch of python scripts that generate povray files and then call povray
  raytracer.
* Povray raytracer generates images.

Example images:

.. figure:: {static}/images/povraygen/x-test-104-fast.png
   :width: 80%
   :alt: Example image.

   Example image.


.. figure:: {static}/images/povraygen/x-test-111-fast.png
   :width: 80%
   :alt: Example image.

   Example image.

FAQ
---

What is ``povray``
******************

Povray is an open-source binary program that uses raytracing to generate very
real looking images.

Images are defined using text files and then rendered.

Why use ``povray``
******************

It's API (i.e. plain-text files) is best and most understandable I found.

Pros:

* Very good documentation;
* Very good quality of produced images;
* I could start experimenting quickly;

Cons:

* Requires me to generate povray images programatically;

I evaluated:

* Blender --- it has a Python API, however this python api is callable from
  **inside** blender application, and is really geared towards writing plugins,
  that is: you need to worry about state of the UI when managing 3d objects
  (for example --- you can't select an object UI needs to be in a state that
  allows selecting objects).

  .. note::

    Above opinion is based on working with blender python API for couple of
    so my experience is *extreamly* limited.

* Panda3d --- it is a nice 3d game engine (allegedly) in Python, however:

  * Tutorial did not explain to me how to put objects and/or how to define them.
  * It is geared towards "game engine" use case --- that is: you load assets
    created in 3d program, and then you move and animate them. I want to
    dynamically generate my assets.

Will you show us the code
*************************

Once I get around to it, I'll release the code :)
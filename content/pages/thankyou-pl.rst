Thanks
======

:date: 2018-04-29 10:20
:slug: thankyou
:lang: pl
:summary: Thank you
:status: hidden

Wielkie dzięki dla twórców oprogramowania z którego korzysta ta strona:

- `Python <https://www.python.org>`__
- `Generator statycznych stron Pelican <https://github.com/getpelican/pelican/>`__
- `Font: The League Mono <https://www.theleagueofmoveabletype.com/league-mono>`__
  (`sprawdźcie inne ich fonty na <https://www.theleagueofmoveabletype.com/>`__)
- `Bootstrap 4.1 <https://getbootstrap.com/docs/4.1/getting-started/introduction/>`__
  (wykorzystano pliki reset css)
- Hostowane na `Gitlab Pages <https://docs.gitlab.com/ee/user/project/pages/>`__

This site is powered by
=======================

:date: 2018-04-29 10:20
:slug: thankyou
:summary: Thank you
:status: hidden


Many thanks to developers of multiple technologies I use in this site:

- `Python <https://www.python.org>`__
- `Pelican static file generator <https://github.com/getpelican/pelican/>`__
- `The League Mono font <https://www.theleagueofmoveabletype.com/league-mono>`__
  (`please see all their fonts <https://www.theleagueofmoveabletype.com/>`__)
- `Bootstrap 4.1 <https://getbootstrap.com/docs/4.1/getting-started/introduction/>`__ (only reset css file used)
- Site is hosted on `Gitlab Pages <https://docs.gitlab.com/ee/user/project/pages/>`__

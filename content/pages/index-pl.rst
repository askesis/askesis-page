O tej stronie
=============

:date: 2018-04-29
:slug: index
:status: hidden
:lang: pl

Cześć! Nazywam się Jacek i jestem dobrym full stackiem.

.. note::

    Linki do bloga są wyżej, najlepiej zacząć od ``developement``.

W trakcie mojej kariery stworzyłem sporo dobrze działających usług, w terminie.
Wykonałem je albo samodzielnie albo jako część nie dużego zespołu.

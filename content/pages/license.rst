License terms
=============

:date: 2018-04-29
:slug: license
:summary: Describes licencing terms for this site.

Licensing rules are as follows: everything on this site is on
`CC BY-ND 4.0 <https://creativecommons.org/licenses/by-nd/4.0/>`__ license, except:

* Articles in `development category </category/developement.html>`__, which are on `CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>`__.
* `Theme is MIT licensed, and downloadable here <https://gitlab.com/askesis/askesis-theme>`__.

`Source code for this site is available here <https://gitlab.com/askesis/askesis-page>`__ (
`here is the contribution guide <{filename}./contributing.rst>`__)

So basically you can share this work as much as you like, as long
as you attribute me as the author. Also you can share updated
versions of any technical blogpost, as long as you attribute
source of this page).

Do see `more about software used on this site here </pages/thankyou.html>`__.
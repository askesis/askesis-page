About this page
===============

:date: 2018-04-29
:slug: index
:status: hidden

Hi I'm Jacek and I'm a seasoned full stack developer.

.. note::

    To get to the blog just click on categories in the header. Best to start
    with ``development``.

In my development career, I developed a lot of working, robust, software,
on deadline, either alone or in a small team.



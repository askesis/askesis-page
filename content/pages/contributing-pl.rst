Contributing
============

:date: 2018-05-04 23:25
:slug: contributing
:lang: pl

**Z chęcią przyjmę kontrybucje!**

Jeśli chcesz poprawić literówkę bądź drobny błąd --- po prostu wrzuć 
Merge Requesta (to pull request po gitlabowemu).
`Repozytorium jest tutaj <https://gitlab.com/askesis/askesis-page>`__.

Jeśli masz ochotę coś przetłumaczyć na inny język (np. na Polski ;) to po prostu
wrzuć Merge Requesta.

Jeśli masz ochotę coś napisać ze mną -- wyślij mejla :)



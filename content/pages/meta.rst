Blog metadata
=============

:date: 2018-05-05 00:11
:slug: metadata

Here I enumerate (and explain) some choices I made on this site.

* **This is a static site** --- html is generated from ``.rst`` files, and then
  served by anything [*]_ . Static site means no need to handle security patching
  of CMSes (which, somehow, always need patching).

  Using restructuredText as markup format means I probably won't loose the
  content I created (I was blogging for a long time --- let's say I've seen
  Blogger go out of beta, and most of my blogs were lost due to me loosing
  interest, and *also* not being able to easily move my content).
* **Self hosted** means no third party with unreadable T&C (technically hosted
  on gitlab pages,  their T&C are bearable). Also no third party tracks you.
* **Not much tracking**: I have added very simple tracker implemented via
  dead simple lambda function.
* **No CDN's and third parties** --- I hate when some site uses javascript from
  ``deadbeef21345.cloudfare.com`` (or similar).
* **No javascript**
* **Limited CSS markup** --- OK I used single webfont.
* **Nice geeky look** --- Well I'm a nerd. And how many monotype pages you know ;)?
* **Looks nice on phones** --- there are a lot of technical sites that just
  work awfully on small screens.






.. [*] Currently hosted on Gitlab Pages.
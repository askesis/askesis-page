Get in touch
============

:date: 2018-05-04 23:25
:slug: contact

Feel free to reach me at my e-mail (``jacek@askesis.pl``),
also I'm `jacek@mastodon.technology <https://mastodon.technology/@jacek>`__,
(if you are into `mastodon <https://joinmastodon.org/>`__ thing.

Also --- since there is no comment section (and no tracking) --- *I encourage you*
to get in touch, as right now this is only means I know someone reads this ;)

.. raw:: html

    <div style="display: none">
        <a rel="me" href="https://mastodon.technology/@jacek">Mastodon</a>
    </div>




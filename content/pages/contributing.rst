Contributing
============

:date: 2018-05-04 23:25
:slug: contributing

**Contributions are welcome!**

If you want to correct factual error, or just do a spelling fix just
post a Merge Request (gitlab word for pull request) on the
`site repository <https://gitlab.com/askesis/askesis-page>`__.

If you want to translate some content --- also just post a Merge Request.

If you want to co-author some contents also feel free to mail me.




